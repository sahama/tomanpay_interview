
from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from accounting.views import BankViewSet, BankRequestViewSet

router = DefaultRouter()
router.register(r'banks', BankViewSet, basename='Bank')
router.register(r'bank_request', BankRequestViewSet, basename="BankRequest")

urlpatterns = [
    path(r'admin/', admin.site.urls),
    path('api/', include(router.urls)),
]
