class Payment(object):

    @staticmethod
    def create_payment(bank_name, payment_method):
        payment_methods = {
            'i': IBANPayment,
            'c': AccountNumberPayment,
            'a': CardNumberPayment
        }
        klass = payment_methods[payment_method]
        payment = klass()
        return payment

    def pay(self, bank_request):
        pass


class IBANPayment(Payment):

    def pay(self, bank_request):
        print("Requesting with IBAN")


class AccountNumberPayment(Payment):

    def pay(self, bank_request):
        print("Requesting with Account Number")


class CardNumberPayment(Payment):

    def pay(self, bank_request):
        print("Requesting with Card Number")

