from rest_framework import serializers
from accounting.models import BankRequest, Bank


class BankSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bank
        fields = '__all__'


class BankRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = BankRequest
        fields = '__all__'


class BankRequestSerializerListRetrieve(serializers.ModelSerializer):
    class Meta:
        model = BankRequest
        fields = '__all__'

    bank = BankSerializer()
