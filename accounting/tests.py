from rest_framework import status
from rest_framework.test import APITestCase
from accounting.models import Bank


class BankRequestTestCase(APITestCase):

    def setUp(self):
        self.bank = Bank.objects.create(name="Mellat", method='i')

    def test_get_banks(self):
        url = '/api/banks/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_bank_request(self):
        url = '/api/bank_request/'
        data = {'bank': self.bank.id,
                'iban': '123',
                "account_number": "5",
                "card_number": "5",
                "amount": "5600"
                }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

