import datetime

from django.db import models, transaction
from django.db.models.signals import post_save
from django.dispatch import receiver

from accounting.payment_strategy import Payment

payment_methods = [
    ("i", "iban"),
    ("c", "card_number"),
    ("a", "account_number"),
]


class Bank(models.Model):
    name = models.CharField(
        max_length=32,
        null=False,
        blank=False,
        help_text="This is the actual bank name",
    )
    method = models.CharField(
        max_length=1,
        null=False,
        blank=False,
        choices=payment_methods,
        help_text="This is the payment method, of which the bank can do the transaction."
    )

    def __str__(self):
        return self.name


class BankRequest(models.Model):
    """
        In this model, we store our logs of requests to banks.
    """
    bank = models.ForeignKey(
        Bank,
        blank=False,
        null=False,
        on_delete=models.DO_NOTHING,
        help_text="This is the bank, which the request is made for."
    )
    iban = models.CharField(
        max_length=32,
        blank=False,
        null=False,
        help_text="The ISBN number of the customer",
    )
    account_number = models.CharField(
        max_length=32,
        blank=False,
        null=False,
        help_text="This is the account number of the customer",
    )
    card_number = models.CharField(
        max_length=16,
        blank=False,
        null=False,
        help_text="This is the card number of the customer",
    )
    amount = models.IntegerField(
        blank=False,
        null=False,
        help_text="The amount of money, that we are transporting for the customer",
    )
    request_id = models.BigIntegerField(
        null=False,
        blank=True,
        help_text="This field should left blank by the users, and fill automatically with the system",
    )
    result = models.BooleanField(
        default=False,
        help_text="This is the response we got from the bank API",
    )
    date = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.request_id:
            try:
                last_id = BankRequest.objects.filter(bank__name=self.bank.name).last().request_id
            except AttributeError:
                last_id = 0
            self.request_id = last_id + 1
        super().save(*args, **kwargs)

    class META:
        unique_together = (('bank_id', 'request_id'),)
        get_latest_by = "request_id"


@receiver(post_save, sender=BankRequest, dispatch_uid="request_to_bank")
def request_to_bank(sender, instance, created, **kwargs):
    """
        After saving the object, we know that our request_id is valid, so in post_save, we can request to bank.
    """
    if created:
        transaction.on_commit(lambda: request_to_bank_mock(instance))


def request_to_bank_mock(obj: BankRequest) -> None:
    """
        This method is a bank api simulation for our tests.
    :param obj:
    :return:
    """

    bank = Bank.objects.get(id=obj.bank_id)
    payment = Payment.create_payment(obj, bank.method)
    payment.pay(obj)
    obj.result = obj.request_id % 2 == 0
    obj.save()

