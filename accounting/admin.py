from django.contrib import admin

from accounting.models import BankRequest, Bank


@admin.register(BankRequest)
class BankRequestAdmin(admin.ModelAdmin):
    readonly_fields = ('request_id', 'date', 'result')
    list_display = ("bank", "result")


@admin.register(Bank)
class BankAdmin(admin.ModelAdmin):
    list_display = ("name",)

