from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser

from accounting.models import BankRequest, Bank
from accounting.serializer import BankRequestSerializer, BankSerializer, BankRequestSerializerListRetrieve


class BankViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Bank.objects.all()
    serializer_class = BankSerializer


class BankRequestViewSet(viewsets.ModelViewSet):
    queryset = BankRequest.objects.all()
    serializer_classes = {
        'retrieve': BankRequestSerializerListRetrieve,
        'list': BankRequestSerializerListRetrieve,
        'other': BankRequestSerializer
    }

    # permission_classes = [IsAdminUser]
    def get_serializer_class(self):
        serializer_class = self.serializer_classes.get(self.action, self.serializer_classes['other'])
        return serializer_class
